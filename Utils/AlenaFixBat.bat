@echo off
@chcp 65001 >NUL
setlocal EnableExtensions EnableDelayedExpansion

rem ensure file is saved as UTF-8
rem space-seperated list of characters to find then replace, character pairs matched between lines by position
set "_findChar=ß с"
set "_replChar=c c"


set _arrayCountFirst=0
for %%a in (%_findChar%) do (
    set /a _arrayCountFirst+=1
    set _findCharArray[!_arrayCountFirst!]=%%a
)

set _arrayCount=0
for %%a in (%_replChar%) do (
    set /a _arrayCount+=1
    set _replCharArray[!_arrayCount!]=%%a
)

if not "%_arrayCount%"=="%_arrayCountFirst%" (
    echo Find and Replace character count differ
    goto :EOF
)
set _actions=0

set "PWD=%~dp0"
set "_findGroup=%_findChar: =%"
set "_foundDir="
for /d /r %%a in (unique_np?) do (
    echo %%a| findstr /r "\\assets\\standalone_content\\pic\\unique_npc$" >NUL
    if errorlevel 1 (
        echo Ignoring %%a
    ) else (
        set _foundDir=%%~dpa
        goto label_check_target
    )
)

:label_check_target
if not "%_foundDir%"=="" (
    for /l %%i in (1,1,%_arrayCount%) do (
        if not "%%i"=="1" set "_charRepr=!_charRepr!, "
        for /f "delims=" %%b in ("!_findCharArray[%%i]!=!_replCharArray[%%i]!") do set "_charRepr=!_charRepr!%%b"
    )
    echo Mappings: !_charRepr!
    echo Working In: %_foundDir%
    choice /C yn /T 10 /D n /N /M "Copy all with corrected name [y/n]? "
    if errorlevel 2 (
        echo No Action Taken
    ) else (
        rem findstr works on unicode iff search strings sourced from file
        echo [%_findGroup%][^^\\]*$>%PWD%findGroup.txt
        
        pushd "%_foundDir%" 2>nul
        if not errorlevel 1 (
            for /f "delims=" %%a in ('dir /b /a-d-h /s ^| findstr /r /g:%PWD%findGroup.txt 2^>NUL') do (
                set "name=%%~na"
                for /l %%i in (1,1,%_arrayCount%) do (
                    for /f "delims=" %%b in ("!_findCharArray[%%i]!=!_replCharArray[%%i]!") do set "name=!name:%%b!"
                )
                if not "!name!"=="%%~na" (
                    set /a _actions+=1
                    copy "%%a" "%%~dpa!name!%%~xa" >NUL
                )                
            )
            popd
        )
        echo !_actions! file(s^) copied.
    )
) else (
    echo No pic folder
)    
pause